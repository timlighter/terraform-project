provider "gitlab" {
    token = "${var.gitlab_token}"
}

resource "gitlab_project" "terraform_project" {
  name        = "terraform-project"
  description = "My awesome terraform codebase"
  default_branch = "master"
  wiki_enabled = "true"
  visibility_level = "public"
}

# Add a deploy key to the project
resource "gitlab_deploy_key" "terraform_deploy_key" {
    project = "timlighter/terraform-project"
    title = "terraform deploy key"
    key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC+l+yXfot7kVeYWp+5JhntKg5/aWU0/SmACLtdxp+W/oDu4C4Di92YM+AgmRD0f6ov1kMmTcENO6/a1HU+xMF/6zN4QFoi7p1HqCDNC6WBGto0BNeOK0JeWWchV4FhePGMXmHO34Suw2vxYHgb9qGrlSLAHb8ApO5YNkimhoRahLOTGt4qcQQvBl6RT8bteGsp0JA/fO3hMfjZv3N4UjB6kt2WlWQm38ZUCR0GlEcDYpokLPnfczPykq94XOK2rpZPo0prwU6wD0JJV4GWifOt4wT8I2qTcxjIosD8cat+OZR1xQMtBF16rFjQKM3IY9PJEFDpXzO1mFCb8OXj1zlL Dom@DESKTOP-PPEUAQN"
    can_push = "true"
}
